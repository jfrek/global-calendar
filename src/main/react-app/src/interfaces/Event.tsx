interface Event {
//    id: 10,
//    title: 'Dinner',
//    start: new Date(2015, 3, 12, 20, 0, 0, 0),
//    end: new Date(2015, 3, 12, 21, 0, 0, 0),
    id: number;
    title: string;
    start: Date;
    end: Date;
}

export default Event;